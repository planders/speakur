# Speakur #

This is a simple forum / chat application using:

* Polymer / W3C Web Components
* Firebase backend

Single page app / routing:
https://github.com/ebidel/polymer-change/blob/master/demos/spa.html

Actually this is more recent:
https://www.polymer-project.org/articles/spa.html

better:
https://github.com/Polymer/docs/blob/master/articles/demos/spa/final.html

# TODO #

paper-shadow not working on main content area for some reason
something to do with core-animated-pages?
https://github.com/Polymer/polymer/releases
"""
<paper-shadow> is a container instead of targetting another element
use setZ() to set the depth of the shadow instead of assigning the z property
z uses a one-time binding
"""

selected tab should have filled in icon.

Initially selected tab isn't 'paper' highlighted...
and animations stops? - until you click a second time
Is this a known issue? the docs just say to use 'noink' to 'fix' this
problem, but no indication of how to obtain the animation.


# Data Model #

Data model notes...

# Developer instructions #

* Install Webstorm or another editor/IDE of your choice
* Install git  (and/or another git client like Sourcetree)
* Install nodejs  
    * Windows package includes npm. Install npm separately if not on Windows.
* **(Windows)** Fix npm command: http://stackoverflow.com/questions/25093276/nodejs-windows-error-enoent-stat-c-users-rt-appdata-roaming-npm
    * run command prompt as admin
    * create the directory: `mkdir c:\Users\USERNAME\AppData\Roaming\npm`
    * run `npm --help`
    * now you can close admin cmd prompt
* install bower:
    * `npm install -g bower`
    * This is a dependency manager for your front-end components
* `npm install -g grunt-cli`
* clone project with git
* cd into that dir
* `npm install`  
* `bower list`
    * should show that a few dependencies are registered but not installed
* `bower update`
    * this will pull down stuff to fulfill the dependencies
* Open WebStorm
    * Use Grunt console and run `serve` task
    * Or just do this from the command line if not using WebStorm
* You can now open in browser like http://localhost:9000

