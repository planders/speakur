(function (document) {
  'use strict';

  document.addEventListener('polymer-ready', function () {
    // Perform some behaviour
    console.log('Polymer is ready to rock!');
  });

  var DEFAULT_ROUTE = 'home';

  var template = document.querySelector('template[is="auto-binding"]');
  var ajax, pages, scaffold;
  // var cache = {};

  template.pages = [
    {name: 'Home', hash: 'home', url: '/pages/home.html'},
    {name: 'Forums', hash: 'forums', url: '/pages/home.html'},
    {name: 'Pinned', hash: 'pinned', url: '/pages/home.html'},
    {name: 'Chats', hash: 'chats', url: '/pages/home.html'},
    {name: 'Friends', hash: 'friends', url: '/pages/home.html'},
    {name: 'Settings', hash: 'settings', url: '/pages/settings.html'},
    {name: 'About', hash: 'about', bottom: true, url: '/pages/about.html'}
  ];

  template.addEventListener('template-bound', function (e) {
    scaffold = document.querySelector('#speakur-scaffold');
    ajax = document.querySelector('#speakur-ajax');
    pages = document.querySelector('#speakur-pages');

/*
    var keys = document.querySelector('#speakur-keys');

    // Allow selecting pages by num keypad. Dynamically add
    // [1, template.pages.length] to key mappings.
    var keysToAdd = Array.apply(null, template.pages).map(function (x, i) {
      return i + 1;
    }).reduce(function (x, y) {
      return x + ' ' + y;
    });
    keys.keys += ' ' + keysToAdd;
*/

    // move to config function?
    this.globals.config = this.$.speakurConfig;
    // this.globals.profile = this.$.speakurProfile.profile;

    this.route = this.route || DEFAULT_ROUTE; // Select initial route.
  });

/*
  template.keyHandler = function (e, detail, sender) {
    // Select page by num key.
    var num = parseInt(detail.key);
    if (!isNaN(num) && num <= this.pages.length) {
      pages.selectIndex(num - 1);
      return;
    }

    switch (detail.key) {
      case 'left':
      case 'up':
        pages.selectPrevious();
        break;
      case 'right':
      case 'down':
        pages.selectNext();
        break;
      case 'space':
        detail.shift ? pages.selectPrevious() : pages.selectNext();
        break;
    }
  };
*/

  template.menuItemSelected = function (e, detail, sender) {
    if (detail.isSelected) {

      // Need to wait one rAF so <core-ajax> has it's URL set.
      this.async(function () {
/*
        if (!cache[ajax.url]) {
          ajax.go();
        }
*/
        ajax.go();

        scaffold.closeDrawer();
      });

    }
  };

  template.ajaxLoad = function (e, detail, sender) {
    e.preventDefault(); // prevent link navigation.
  };

  template.onResponse = function (e, detail, sender) {
    var article = detail.response.querySelector('body');
    var html = article.innerHTML;

    // cache[ajax.url] = html; // Primitive caching by URL.

    this.injectBoundHTML(html, pages.selectedItem.querySelector('#page-content'));
  };

  template.login = function() {
    console.log('LOGIN!');
    this.$.speakurLoginDialog.toggle();
  };

  template.logout = function() {
    this.$.baseLogin.logout();
    this.doLogout();
  };

  template.onLogin = function() {
    this.globals.currentUser = this.user;

    // Look for both Google and FB info
    this.$.speakurProfile.uid = this.user.uid;
    this.async(function() {
      this.$.speakurProfile.checkProfile(this.user);
    }, null, 1300); /// questionable... race condition could overwrite existing? check this


    // only close this if open! TODO: don't violate encapsulation here... make it a reflected property
    if (this.$.speakurLoginDialog.$['login-dialog'].opened) {
      this.$.speakurLoginDialog.toggle();
    }

  };

  template.doLogout = function() {
    // clear out the global user/profile info
    this.globals.currentUser = null;
    this.$.speakurProfile.uid = null;
    this.globals.profile = null;
  };

  template.onLoginError = function(err) {
    this.doLogout();
    console.log('An error occurred.');
    // Show some kind of error dialog / message?
  };


// wrap document so it plays nice with other libraries
// http://www.polymer-project.org/platform/shadow-dom.html#wrappers
})(wrap(document));
